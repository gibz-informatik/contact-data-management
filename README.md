﻿# Lernkontrolle M226A

Mit dieser Lernkontrolle werden die Handlungskompetenzen im Modul M226A gemäss Modulidentifikation überprüft.

Die Verwendung von Hilfsmitteln für die Bearbeitung dieser Lernkontrolle ist erlaubt. Sie dürfen sämtliche Unterlagen des Moduls, die erstellten Videos von Ihnen und den anderen Lernenden aus Ihrer Klasse, sämtliche Übungsaufgaben und (wo vorhanden) die Musterlösungen sowie Inhalte aus dem Internet verwenden. **Explizit verboten ist die (analoge oder digitale) Zusammenarbeit mit anderen Personen - bearbeiten Sie sämtliche Teile dieser Lernkontrolle <u>alleine</u>.**

## Kontaktdatenverwaltung

Erstellen Sie ein Programm für die **Verwaltung von Kontaktdaten** eines Unternehmens. Beachten Sie dabei die folgenden *funktionalen* und *nicht-funktionalen* Anforderungen für dieses Programm.

☝️ **Sie sollen die *Applikation* sowie das dazugehörige *Klassendiagramm* erstellen.**

**Tipp:** Erstellen Sie das *Klassendiagramm zuerst* und schreiben Sie *anschliessend den Quellcode* der Applikation. Schauen Sie sich zudem vor dem Entwurf des Klassendiagramms die bereits vorhandenen Unit Tests an - daraus können/müssen Sie einige Klassen- und Methodennamen ableiten. Allenfalls passen Sie am Schluss das Klassendiagramm an, falls Sie während der Implementierung vom ursprünglichen Konzept abgewichen sind.

### Funktionale Anforderungen

- Für *alle* Personen relevant sind `Vorname`, `Nachname`, `Geburtsdatum` sowie das `Geschlecht` 
- Bei den Personen werden *interne* und *externe* Personen unterschieden:
  - Bei *internen* Personen können zusätzlich die `Abteilung` sowie das `Eintrittsdatum` erfasst werden.
  - Bei *externen* Personen können zusätzlich der Name vom `Unternehmen`  erfasst werden.
- Für *alle* Personen können verschiedene Arten von Kontaktdaten erfasst werden:
  - `E-Mail Adresse`
  - `Telefonnummer` (Unterscheidung *Festnetz* und *Mobilnetz*)
  - `Postadresse` (bestehend aus `Strasse`, `Postleitzahl` und `Ort`)
- Für jede Person sollen für alle drei Arten von Kontaktdaten jeweils beliebig viele Informationen erfasst werden können. Zum Beispiel also 2 E-Mail Adressen, 3 Telefonnummern und 0 Postadressen.
- Für jede Art von Kontaktdaten soll jeweils eine Information als `Primärinformation` festgelegt werden können. Im obigen Beispiel kann folglich 1 Haupt-Telefonnummer von den insgesamt 3 Telefonnummern als Primärinformation definiert werden.
- Es soll möglich sein, alle Informationen (inkl. Kontaktdaten) einer beliebigen Person möglichst einfach (und übersichtlich) auf der Konsole auszugeben (Hinweis: Überschreibung einer *Verwaltungsmethode*).
  *__Hinweis:__ Die Ausgabe an sich ist kein Bestandteil der Aufgabe - lediglich die Implementation einer Methode, welche dafür genutzt werden kann.*

Weitere funktionale Anforderungen sind aus den vorhandenen Testfällen in der Klasse `ContactDataManagement.Tests.GivenTests` abzuleiten! Informationen zum Testing sind weiter unten, im Abschnitt *Testing* aufgeführt.

### Nicht-funktionale Anforderungen

- Das Klassendiagramm ist **inhaltlich** und **formal** korrekt **mit UML** dargestellt. Das Werkzeug zur Erstellung des Diagramms ist frei wählbar.
- Die Applikation ist mit **C#** als **Konsolen-Projekt** angelegt (kein GUI erforderlich). Vorzugsweise verwenden Sie die aktuellste Version von *.NET (Core)*.
- Es ist **keine Interaktivität** gefordert. Es müssen folglich keine Eingaben abgefragt, validiert oder verarbeitet werden.
- Die Funktionsweise soll **exemplarisch** (d.h. anhand einiger Beispiele) in der `Main`-Methode der Klasse `Program` gezeigt werden. Instanzieren Sie dazu einige Objekte der von Ihnen erstellten Klassen.

### Testing (Unit Tests = Komponententests)

Die Projektvorlage besteht aus zwei Projekten: `ContactDataManagement` für die Implementation des produktiven Codes (funktionale Anforderungen) und `ContactDataManagementTests` für die Implementation der Unit Tests. Die Komponententests sollen relevante Aspekte der Kontaktdatenverwaltung automatisiert auf ihre korrekte Funktionsweise überprüfen.

#### Bestehende Tests

In der Klasse `ContactDataManagement.Tests.GivenTests` existieren bereits einige vorbereitete Testmethoden. Diese Testmethoden haben im Kontext dieser Lernkontrolle die Funktion von **funktionalen Anforderungen**. Sie sollen Ihren Programmcode so schreiben, dass bei der Ausführung der Unit Tests möglichst viele (alle) dieser Tests erfolgreich sind. Der Inhalt der Klasse `ContactDataManagement.Tests.GivenTests` (bestehende Tests) darf *nicht* verändert werden!

#### Eigene Tests

Zusätzlich zu den bestehenden Tests sollen Sie in der Klasse `ContactDataManagement.Tests.OwnTests` eigene Komponententests implementieren. Implementieren Sie folgende Tests:

1. Bei einer Instanz der Klasse `Person` (oder Subklasse davon) wird eine primäre Telefonnummer definiert (ohne diese Telefonnummer zuvor anderweitig hinzuzufügen). Durch das Hinzufügen der primären Kontaktinformation soll für die Person anschliessend genau eine Telefonnummer erfasst sein. Zudem soll die Nummer dieser Telefonnummer identisch mit der zuvor erfassten, primären Telefonnummer sein.
2. Bei einer Instanz der Klasse `Person` (oder Subklasse davon) soll geprüft werden, ob die Methode `AddContactData` mit Parametern unterschiedlicher Datentypen korrekt funktioniert. Dazu werden nacheinander, d.h. in aufeinanderfolgenden Ausführungen der gleichen Methode, Kontaktinformationen unterschiedlicher Datentypen mit der Methode `AddContactData` erfasst. Anschliessend soll geprüft werden, ob die Summe der Anzahl E-Mail Adressen, Telefonnummern und Postadressen insgesamt genau `1` ergibt.
   Beispiele:
   1. Ausführung 1: Hinzufügen einer E-Mail Adresse --> Anzahl E-Mail Adressen + Anzahl Telefonnummern + Anzahl Postadressen = 1
   2. Ausführung 2: Hinzufügen einer Telefonnummer --> Anzahl E-Mail Adressen + Anzahl Telefonnummern + Anzahl Postadressen = 1
   3. Ausführung 3: Hinzufügen einer Postadresse --> Anzahl E-Mail Adressen + Anzahl Telefonnummern + Anzahl Postadressen = 1

### Beurteilungskriterien

Grundsätzlich sollen Sie die Applikation so implementieren, dass Ihre fachliche Kompetenz als Applikationsentwicklerin bzw. Applikationsentwickler gut erkennbar ist. Im Kontext des Modul 226A sind dabei primär die objektorientierten Konzepte der **Kapselung**, **Vererbung** und **Polymorphie** relevant. Zudem soll das Klassendiagramm der offiziellen Notation von UML entsprechen und fachlich sowie inhaltlich korrekt sein.

Die nachfolgende Liste hebt einige wesentliche Aspekte hervor, ist jedoch nicht abschliessend:

- Das **Design der Klassen** ist sinnvoll gewählt (Anzahl/Grösse der Klassen, Vererbungshierarchie).
- Das Prinzip der **Kapselung** wurde fachlich korrekt umgesetzt.
- **Vererbung** wird sinnvoll und gezielt angewendet - keine unnötig komplexen Strukturen.
- **Polymorphie** wird zur gezielten Vereinfachung eingesetzt (Klassendesigns, Nutzbarkeit/Flexibilität der Klassen).
- Die geforderten **Kardinalitäten** gemäss den funktionalen Anforderungen werden sowohl im Klassendiagramm als auch im Quellcode korrekt umgesetzt.
- Das **Klassendiagramm** ist inhaltlich und formal korrekt sowie sauber und übersichtlich als UML-Diagramm dargestellt.
- Der Quellcode der Applikation ist gut **verständlich, wartbar und erweiterbar**.
- Die **Komponententests** sind lauffähig und bei der Ausführung erfolgreich.
- Die selber erstellten Komponententests sind technisch sauber implementiert und strukturiert.