﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ContactDataManagement.Tests
{
    [TestClass]
    public class GivenTests
    {
        [TestMethod]
        public void AddPostalAddressTest()
        {
            // Arrange
            PostalAddress postalAddress = new PostalAddress { Street = "Baarerstrasse 100", ZipCode = 6301, City = "Zug" };
            Person person = new ExternalPerson();

            // Act
            person.AddPostalAddress(postalAddress);

            // Assert
            Assert.AreEqual(1, person.PostalAddresses.Count);
        }

        [TestMethod]
        public void AddEmailAddressAsContactDataTest()
        {
            // Arrange
            EmailAddress emailAddress = new EmailAddress { Email = "user@domain.com" };
            Person person = new ExternalPerson();

            // Act
            person.AddContactData(emailAddress);

            // Assert
            Assert.AreEqual(1, person.EmailAddresses.Count);
        }

        [TestMethod]
        public void AutomaticallySetFirstEmailAddressAsPrimaryTest()
        {
            // Arrange
            Person person = new ExternalPerson();
            string primaryEmailAddressString = "user@domain.com";
            ContactData primaryEmailAddress = new EmailAddress { Email = primaryEmailAddressString };

            // Act
            person.AddContactData(primaryEmailAddress);
            person.AddContactData(new EmailAddress { Email = "otherUser@otherDomain.com" });

            // Assert
            Assert.AreEqual(primaryEmailAddressString, person.PrimaryEmailAddress.Email);
        }
    }
}